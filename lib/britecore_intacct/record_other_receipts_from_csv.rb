module BritecoreIntacct
	class RecordOtherReceiptsFromCsv
		def self.record_from filename, payment_method="Check"
			rows=CSV.read(filename)
			#rows.shift() #remove headers
			puts "creating other receipts #{rows.size()} to intacct"
			rows.each do |row|
				d_result = row[0].split(' ')[0]
				d = Date.strptime(d_result, '%Y-%m-%d')
  				receipt = BritecoreIntacct::RecordOtherreceipt.new
				receipt.payment_on = d
				receipt.received_on = d
				receipt.payee = "Premium"
				receipt.paymentmethod = payment_method
	  			receipt.undeposited_funds_account = "1115.000"
				receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5010.000', amount: row[6] ) )
				receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5011.000', amount: row[7] ) )
				receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5012.000', amount: row[8] ) )
				receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5013.000', amount: row[9] ) )
				receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '9022.000', amount: row[2] ) )
				receipt.send
			end
		end
	end
end
