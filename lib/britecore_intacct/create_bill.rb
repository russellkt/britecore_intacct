module BritecoreIntacct
	class CreateBill < IntacctTransaction
		attr_accessor :vendorid, :created_year, :created_month, :created_day, :due_year, :due_month, :due_day,
			          :lineitems, :billno

		def created_on= created_date
			self.created_year= created_date.year
			self.created_month= created_date.month
			self.created_day= created_date.day
		end

		def due_on= due_date
			self.due_year= due_date.year
			self.due_month= due_date.month
			self.due_day= due_date.day
		end

		def to_xml
			%{ <create_bill>
  			     <vendorid>#{vendorid}</vendorid>
                 <datecreated>
                 	<year>#{created_year}</year>
                 	<month>#{created_month}</month>
                 	<day>#{created_day}</day>
                 </datecreated>
                 <datedue>
                 	<year>#{due_year}</year>
                 	<month>#{due_month}</month>
                 	<day>#{due_day}</day>
                 </datedue>
                 <billno>#{billno}</billno>
                 <billitems>
                 	#{lineitems_xml}
                 </billitems>
				</create_bill> }
		end

		def add_lineitem lineitem
			self.lineitems.push(lineitem)
		end

		def lineitems_xml
			lineitems.map{|li| li.to_xml}.join(" ")
		end
	end
end
