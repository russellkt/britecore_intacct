module BritecoreIntacct
	class GetBill < IntacctTransaction
		attr_accessor :vendorid, :created_year, :created_month, :created_day, :due_year, :due_month, :due_day,
			          :lineitems, :billno, :when_created

		def created_on= created_date
			self.created_year= created_date.year
			self.created_month= created_date.month
			self.created_day= created_date.day
		end

		def due_on= due_date
			self.due_year= due_date.year
			self.due_month= due_date.month
			self.due_day= due_date.day
		end

		def to_xml
			%{ <query>
                    <object>APBILL</object>
                    <select>
                        <field>RECORDNO</field>
                        <field>RECORDID</field>
                        <field>VENDORID</field>
                        <field>VENDORNAME</field>
                        <field>TOTALDUE</field>
                        <field>WHENCREATED</field>
                    </select>
                    <filter>
                        <and>
                            <equalto>
                                <field>VENDORID</field>
                                <value>#{vendorid}</value>
                            </equalto>
                            <greaterthan>
                                <field>WHENCREATED</field>
                                <value>#{when_created}</value>
                            </greaterthan>
                        </and>
                    </filter>
                </query> }
		end
	end
end