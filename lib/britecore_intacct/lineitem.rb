module BritecoreIntacct
	class Lineitem
		attr_accessor :glaccountno, :amount, :memo

		def initialize params = {}
    		params.each { |key, value| send "#{key}=", value }
			@memo ||= ''
  		end

		def to_xml
			%{ <lineitem>
				   <glaccountno>#{glaccountno}</glaccountno>
				   <amount>#{amount}</amount>
					 <memo>#{memo}</memo>
         </lineitem> }
		end
	end
end
