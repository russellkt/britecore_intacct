module BritecoreIntacct
	class IntacctRequest
		include HTTParty
  		base_uri "https://api.intacct.com/ia/xml"

  		attr_accessor :body

        def to_xml
            %{<?xml version="1.0" encoding="iso-8859-1"?>
<request>
 <control>
  <senderid>#{configatron.senderid}</senderid>
  <password>#{configatron.password}</password>
  <controlid>ControlIdHere</controlid>
  <uniqueid>false</uniqueid>
  <dtdversion>3.0</dtdversion>
 </control>
 <operation>
  <authentication>
   <login>
    <userid>#{configatron.userid}</userid>
    <companyid>#{configatron.companyid}</companyid>
    <password>#{configatron.intacct_password}</password>
   </login>
  </authentication>
<content>
 <function controlid="testControlId">
  #{body}
 </function>
</content>
 </operation>
</request>}
		end

  		def send
  			response = self.class.post("/xmlgw.phtml",
  									{  body:  self.to_xml,
                              headers: { 'Content-type' => 'x-intacct-xml-request' }
                           } )
  		end
	end
end
