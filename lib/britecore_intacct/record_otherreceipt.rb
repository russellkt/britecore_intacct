module BritecoreIntacct
	class RecordOtherreceipt

		attr_accessor :payment_year, :payment_month, :payment_day, :received_year, :received_month, :received_day,
			          :lineitems, :payee, :paymentmethod, :undeposited_funds_account

	    def initialize
	    	self.lineitems = []
    	end

		def payment_on= payment_on
			self.payment_year= payment_on.year
			self.payment_month= payment_on.month
			self.payment_day= payment_on.day
		end

		def received_on= received_on
			self.received_year= received_on.year
			self.received_month= received_on.month
			self.received_day= received_on.day
		end

		def to_xml
			%{<record_otherreceipt>
            	<paymentdate>
                	<year>#{payment_year}</year>
                	<month>#{payment_month}</month>
                	<day>#{payment_day}</day>
            	</paymentdate>
            	<payee>#{payee}</payee>
            	<receiveddate>
                	<year>#{received_year}</year>
                	<month>#{received_month}</month>
                	<day>#{received_day}</day>
            	</receiveddate>
            	<paymentmethod>#{paymentmethod}</paymentmethod>
            	<undepglaccountno>#{undeposited_funds_account}</undepglaccountno> <!-- or <bankaccountid></bankaccountid> -->
            	<receiptitems>
            		#{lineitems_xml}
            	</receiptitems>
        	</record_otherreceipt> }
		end

		def send
			request = BritecoreIntacct::IntacctRequest.new
			request.body = to_xml
			puts request.to_xml
			response = request.send
			puts response
		end

		def lineitems_xml
			lineitems.map{|li| li.to_xml}.join(" ")
		end

		def add_lineitem lineitem
			self.lineitems.push(lineitem)
		end
	end
end
