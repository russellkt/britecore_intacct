module BritecoreIntacct
	class CreateVendor < IntacctTransaction
		attr_accessor :vendorid, :name, :onetime, :contactname, :printas, :companyname,
					  :address1, :address2, :city, :state, :zip

		def to_xml
			%{ <create_vendor>
  			     <vendorid>#{vendorid}</vendorid>
                 <name>#{name}</name>
				 <onetime>#{onetime}</onetime>
                 <contactinfo>
                   <contact>
                    <contactname>#{contactname}</contactname>
                    <printas>#{printas}</printas>
                    <mailaddress>
                      <address1>#{address1}</address1>
                      <address2>#{address2}</address2>
                      <city>#{city}</city>
                      <state>#{state}</state>
                      <zip>#{zip}</zip>
											<country>United States</country>
                  	</mailaddress>
                   </contact>
                 </contactinfo>
				</create_vendor> }
		end

	end
end
