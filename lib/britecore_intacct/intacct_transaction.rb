module BritecoreIntacct
  class IntacctTransaction
    def send
      request = BritecoreIntacct::IntacctRequest.new
      request.body = to_xml
      response = request.send
      return response
    end

    def request_xml
      request = BritecoreIntacct::IntacctRequest.new
      request.body = to_xml
      request.to_xml
    end    
  end
end
