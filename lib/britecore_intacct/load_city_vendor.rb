require 'britecore_intacct'

class CityVendor
  attr_accessor :vendorid, :full_name, :address1, :address2, :address3, :zip_code

  def address_line2
    a = address3 ? address2.strip : ''
  end

  def city
    line = address3 ? address3 : address2
    line.split(', ')[0].gsub(/alabama/i,'').strip
  end

  def state
    'AL'
  end

  def to_s
    puts "#{city} #{state} #{zip_code}"
  end
end


csv = CSV.read("/Users/kevinrussell/Google Drive/BMIC/Accountant/2015/Year End/2015 City Taxes.csv")
csv.shift
vendors = []
csv.each do |row|
  vendor = CityVendor.new
  vendor.vendorid=row[0]
  vendor.full_name = row[4]
  vendor.address1 = row[5]
  vendor.address2 = row[6]
  vendor.address3 = row[7]
  vendor.zip_code = row[8]
  vendors << vendor
end

vendors.each do |vendor|
  puts "sending #{vendor}"
  req = BritecoreIntacct::CreateVendor.new
  req.vendorid = vendor.vendorid
  req.printas = vendor.full_name
  req.name = vendor.full_name
  req.onetime = "true"
  req.contactname = vendor.full_name
  req.address1 = vendor.address1
  req.address2 = vendor.address_line2
  req.city = vendor.city
  req.state = vendor.state
  req.zip = vendor.zip_code
  req.send
end
