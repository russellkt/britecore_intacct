require 'test_helper'

class IntacctRequestTest < Minitest::Test
  
  def test_send_request
    request = BritecoreIntacct::IntacctRequest.new
    request.body = '<get_list object="glaccount"></get_list>'
    response = request.send
    assert_equal(response["response"]["operation"]["result"]["status"], "success")
  end
  
end