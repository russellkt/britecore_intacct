require 'test_helper'

class CreateBillTest < Minitest::Test
  
  def test_to_xml
    assert true
  end

  def test_line_items_xml
  	bill = BritecoreIntacct::CreateBill.new
  	line_item1 = BritecoreIntacct::Lineitem.new(glaccountno: '8800', amount: '1000.00')
  	line_item2 = BritecoreIntacct::Lineitem.new(glaccountno: '8801', amount: '250.00')
  	bill.lineitems = [line_item1,line_item2]
  	assert_equal( bill.lineitems_xml.split.join, 
  		          %{<lineitem><glaccountno>8800</glaccountno><amount>1000.00</amount></lineitem><lineitem><glaccountno>8801</glaccountno><amount>250.00</amount></lineitem>} )
  end

  def test_created_on=
  	bill = BritecoreIntacct::CreateBill.new
  	bill.created_on= Date.new(2015,11,1)
  	assert_equal(bill.created_year, 2015)
  	assert_equal(bill.created_month, 11)
  	assert_equal(bill.created_day, 1)
  end

  def test_created_on=
  	bill = BritecoreIntacct::CreateBill.new
  	bill.due_on= Date.new(2015,11,2)
  	assert_equal(bill.due_year, 2015)
  	assert_equal(bill.due_month, 11)
  	assert_equal(bill.due_day, 2)
  end

  def test_to_xml
  	bill = BritecoreIntacct::CreateBill.new
  	bill.vendorid = 't10002'
  	line_item1 = BritecoreIntacct::Lineitem.new(glaccountno: '6440', amount: '1000.00')
  	line_item2 = BritecoreIntacct::Lineitem.new(glaccountno: '6450', amount: '250.00')  
  	bill.lineitems = [line_item1,line_item2]
  	bill.created_on= Date.new(2015,11,1)	
  	bill.due_on= Date.new(2015,11,2)
  	bill.billno = 10001
  	bill.send
  	assert true
  end
end