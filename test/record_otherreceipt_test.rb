require 'test_helper'

class RecordOtherReceiptTest < Minitest::Test
  
  def test_to_xml
    assert true
  end

  def test_to_xml
  	receipt = BritecoreIntacct::RecordOtherreceipt.new
  	receipt.payment_on = Date.new(2015,11,30)
  	receipt.received_on = Date.new(2015,11,30)
  	receipt.payee = 'Premium'
  	receipt.paymentmethod = "Check"
  	receipt.undeposited_funds_account = "1115"
  	receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5010', amount: '1000.00') )
    receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5011', amount: '1000.00') )    
    receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5012', amount: '1000.00') )
    receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '5013', amount: '1000.00') )
    receipt.add_lineitem( BritecoreIntacct::Lineitem.new(glaccountno: '9022', amount: '1000.00') )
  	# receipt.send
  	puts receipt.to_xml
  	assert true
  end
end