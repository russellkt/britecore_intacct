require 'test_helper'

class CreateVendorTest < Minitest::Test
  
  def test_to_xml
    vendor = BritecoreIntacct::CreateVendor.new
    vendor.vendorid = "t10000"
    vendor.printas = "Kevin Russell"
    assert true
  end

  def test_send
  	i = 2
    vendor = BritecoreIntacct::CreateVendor.new
    vendor.vendorid = "t1000#{i}"
    vendor.printas = "Kevin Russell"
    vendor.name = "Kevin Russell-t1000#{i}"
    vendor.contactname = "Kevin Russell-t1000#{i}-"
    vendor.address1 = "315 E Laurel Ave"
    vendor.address2 = "Suite 102"
    vendor.city = "Foley"
    vendor.state = "Alabama"
    vendor.zip = "36535"
    vendor.send
    assert true
  end
end